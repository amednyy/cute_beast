#pragma once

#include <QtCore/qglobal.h>

#if defined(QTBEASTSERVER_LIBRARY)
#    define QTBEASTSERVER_EXPORT Q_DECL_EXPORT
#else
#    define QTBEASTSERVER_EXPORT Q_DECL_IMPORT
#endif
