#ifndef CUTE_BEAST_LOGGING_HPP
#define CUTE_BEAST_LOGGING_HPP

#ifdef CUTE_BEAST_LOGGING

#    include <spdlog/fmt/fmt.h>
#    include <spdlog/sinks/stdout_color_sinks.h>
#    include <spdlog/spdlog.h>

#    ifndef CUTE_BEAST_MAKE_LOGGER
template <class... Args> std::shared_ptr<spdlog::logger> cute_beast_make_default_logger(const char* name, Args&&... args)
{
    auto logger = spdlog::stderr_color_mt(fmt::format(name, std::forward<Args&&>(args)...));
    logger->set_level(spdlog::level::trace);
    return logger;
}
#        define CUTE_BEAST_MAKE_LOGGER(...) cute_beast_make_default_logger(__VA_ARGS__)
#    endif
#    define CUTE_BEAST_LOG_ERROR(logger, ...) SPDLOG_LOGGER_ERROR(logger, __VA_ARGS__)
#    define CUTE_BEAST_LOG_INFO(logger, ...) SPDLOG_LOGGER_INFO(logger, __VA_ARGS__)
#    define CUTE_BEAST_LOG_TRACE(logger, ...) SPDLOG_LOGGER_TRACE(logger, __VA_ARGS__)
#    define CUTE_BEAST_LOG_DEBUG(logger, ...) SPDLOG_LOGGER_DEBUG(logger, __VA_ARGS__)
#    define CUTE_BEAST_LOG_CRITICAL(logger, ...) SPDLOG_LOGGER_CRITICAL(logger, __VA_ARGS__)
#    define CUTE_BEAST_LOG_WARN(logger, ...) SPDLOG_LOGGER_WARN(logger, __VA_ARGS__)
#    define CUTE_BEAST_LOGGER_T std::shared_ptr<spdlog::logger>

template <> struct fmt::formatter<boost::string_view> : fmt::formatter<std::string_view> {
    // parse is inherited from formatter<string_view>.
    template <typename FormatContext> auto format(boost::string_view sv, FormatContext& ctx)
    {
        return formatter<std::string_view>::format(std::string_view(sv.data(), sv.size()), ctx);
    }
};

#else
#    ifndef CUTE_BEAST_MAKE_LOGGER

inline void* cute_beast_make_default_logger(...)
{
    return nullptr;
}
#        define CUTE_BEAST_MAKE_LOGGER(...) cute_beast_make_default_logger(__VA_ARGS__)
#    endif
#    define CUTE_BEAST_LOG_ERROR(logger, ...) (void)0
#    define CUTE_BEAST_LOG_INFO(logger, ...) (void)0
#    define CUTE_BEAST_LOG_TRACE(logger, ...) (void)0
#    define CUTE_BEAST_LOG_DEBUG(logger, ...) (void)0
#    define CUTE_BEAST_LOG_CRITICAL(logger, ...) (void)0
#    define CUTE_BEAST_LOG_WARN(logger, ...) (void)0
#    define CUTE_BEAST_LOGGER_T void*

#endif

#endif //CUTE_BEAST_LOGGING_HPP
