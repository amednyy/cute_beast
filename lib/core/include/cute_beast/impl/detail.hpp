#ifndef CUTE_BEAST_SESSION_DETAIL_HPP
#define CUTE_BEAST_SESSION_DETAIL_HPP

#include "common.hpp"
#include <boost/asio/async_result.hpp>
#include <boost/asio/compose.hpp>
#include <boost/asio/coroutine.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/execution/allocator.hpp>
#include <boost/asio/execution/context.hpp>
#include <boost/asio/execution/relationship.hpp>
#include <boost/asio/execution_context.hpp>
#include <boost/asio/ip/network_v4.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/core/buffer_traits.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/callable_traits.hpp>
#include <boost/noncopyable.hpp>
#include <boost/url/url_view.hpp>
#include <charconv>
#include <cstddef>
#include <iostream>
#include <mutex>
#include <optional>
#include <regex>
#include <shared_mutex>
#include <tuple>
#include <type_traits>
#include <utility>

namespace cute_beast
{

namespace ct = boost::callable_traits;
using http::async_write;

/**
* add T convert(string_view) to types namespace for your own route handler argument types
* eq: boost::uuid convert(string_view uuid,error_code&e)
*/
namespace types
{
    // A converter for objects of type T.
    template <typename T, typename Enable = void> struct converter {
        // A deleted default constructor indicates a disabled formatter.
        converter() = delete;
    };
    template <class T> struct converter<T, std::enable_if_t<std::is_integral_v<T>>> {
        converter() = default;
        T convert(std::string&& s, error_code& e)
        {
            typename std::decay<T>::type result;
            auto [p, ec] = std::from_chars(s.begin(), s.end(), result);
            e = std::make_error_code(ec);
            return result;
        }
    };
    template <class T> struct converter<T, std::enable_if_t<std::is_same_v<T, std::string>>> {
        converter() = default;
        T convert(std::string&& s, error_code& e)
        {
            e = {};
            return s;
        }
    };
} // namespace types

namespace detail
{
    template <class Session, class BodyT> auto release_request(Session& session)
    {
        return static_cast<http::request_parser<BodyT>*>(session.parser_.get())->release();
    }
    template <class Session, class BodyT> const auto& get_request(Session& session)
    {
        return static_cast<http::request_parser<BodyT>*>(session.parser_.get())->get();
    }
    template <class BodyT, class Session> auto parser_ptr(Session& session)
    {
        return static_cast<const http::request_parser<BodyT>*>(session.parser_.get());
    }
    template <class Session> auto& session_match_result(Session& session)
    {
        return session.match_result_;
    }
    template <class Session> void session_set_url(Session& session, url_view&& url)
    {
        session.url_ = std::move(url);
    }

    template <class BodyT, class FromBodyT, class Session> void morph_parser(Session& session)
    {
        if constexpr (!std::is_same<BodyT, FromBodyT>::value) {
            auto& parser = session.parser_;
            auto real_ptr = static_cast<http::request_parser<FromBodyT>*>(parser.get());
            parser = std::make_unique<http::request_parser<BodyT>>(std::move(*real_ptr));
        }
    }
    template <class Match> void variadic_convert(size_t /*idx*/, Match&& /*m*/, error_code& /*e*/)
    {
    }

    template <class Match, class Arg, class... Args> void variadic_convert(size_t idx, Match&& m, Arg&& arg, Args&&... args, error_code& e)
    {

        arg = types::converter<std::decay_t<Arg>>().convert(m[idx].str(), e);
        if (e) {
            return;
        }
        variadic_convert(idx + 1, m, args..., e);
    }

    template <class Head, class... Tail> std::tuple<Tail...> tuple_tail(const std::tuple<Head, Tail...>& t)
    {
        return std::apply([](auto&& head, auto&&... tail) { return std::make_tuple(tail...); }, t);
    }
    template <class Handler> auto handler_tail_args(Handler&& handler)
    {
        return std::apply([](int session, int ctx, auto... tail) { return std::make_tuple<tail...>(); }, std::tuple<Handler> { handler });
    }

    template <typename T> struct function_traits : public function_traits<decltype(&T::operator())> {
    };
    // For generic types, directly use the result of the signature of its 'operator()'

    template <typename ClassType, typename ReturnType, typename... Args>
    struct function_traits<ReturnType (ClassType::*)(Args...) const>
    // we specialize for pointers to member function
    {
        enum { arity = sizeof...(Args) };
        // arity is the number of arguments.

        typedef ReturnType result_type;

        template <size_t i> struct arg {
            typedef typename std::tuple_element<i, std::tuple<Args...>>::type type;
            // the i-th argument is equivalent to the i-th tuple element of a tuple
            // composed of those arguments.
        };
    };
    template <typename T> struct function_traits_from_third : public function_traits_from_third<decltype(&T::operator())> {
    };
    template <typename ClassType, typename ReturnType, typename Arg1, typename Arg2, typename... Args>
    struct function_traits_from_third<ReturnType (ClassType::*)(Arg1, Arg2, Args...) const>
    // we specialize for pointers to member function
    {
        enum { arity = sizeof...(Args) };
        // arity is the number of arguments.

        typedef ReturnType result_type;

        template <size_t i> struct arg {
            typedef typename std::tuple_element<i, std::tuple<Args...>>::type type;
            // the i-th argument is equivalent to the i-th tuple element of a tuple
            // composed of those arguments.
        };
    };
    constexpr uint64_t make_methods_mask(std::initializer_list<http::verb> methods)
    {
        uint64_t result = 0;
        for (auto method : methods) {
            result |= 1 << static_cast<std::underlying_type_t<http::verb>>(method);
        }
        return result;
    }
    template <typename> struct is_message : std::false_type {
    };

    template <bool isRequest, class Body, class Fields> struct is_message<http::message<isRequest, Body, Fields>> : std::true_type {
    };
    template <class T>
    constexpr auto is_body(T* body)
        -> decltype(http::is_body_reader<typename T::reader>::value, http::is_body_writer<typename T::writer>::value, std::true_type {})
    {
        return {};
    }
    template <class T> constexpr auto is_body(T* /*body*/) -> std::false_type
    {
        return {};
    }

    struct error_handler_interface {
        virtual void on_exception(const std::exception& e) = 0;
        virtual void on_error(const error_code& ec) = 0;
    };
    template <class HandlerT> struct handler_wrapper {
        HandlerT handler;
        error_handler_interface* error_handler;

        explicit handler_wrapper(HandlerT h, error_handler_interface* eh)
            : handler(std::move(h))
            , error_handler(eh)
        {
        }
        template <class... Args> auto operator()(Args&&... args) const -> decltype(handler(std::forward<Args>(args)...))
        {
            try {
                handler(std::forward<Args>(args)...);
            } catch (const system_error& e) {
                error_handler->on_error(e.code());
            } catch (const std::exception& e) {
                error_handler->on_exception(e);
            } catch (...) {
                std::runtime_error e("unknown exception");
                error_handler->on_exception(e);
            }
        }
    };
    template <class HandlerT> static handler_wrapper<HandlerT> make_handler(HandlerT&& handler, error_handler_interface* eh)
    {
        return handler_wrapper<HandlerT>(handler, eh);
    }
} // namespace detail

} // namespace cute_beast
#endif //CUTE_BEAST_SESSION_DETAIL_HPP
