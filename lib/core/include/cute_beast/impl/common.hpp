#ifndef CUTE_BEAST_COMMON_HPP
#define CUTE_BEAST_COMMON_HPP

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/url/url_view.hpp>

namespace cute_beast
{
namespace beast = boost::beast; // from <boost/beast.hpp>
namespace http = beast::http; // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio; // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp; // from <boost/asio/ip/tcp.hpp>
namespace ssl = boost::asio::ssl; // from <boost/asio/ssl.hpp>
using error_code = beast::error_code;
using system_error = beast::system_error;
using string_view = beast::string_view;
using url_view = boost::urls::url_view;
} // namespace cute_beast

#endif //CUTE_BEAST_COMMON_HPP
