#ifndef CUTE_BEAST_EXCEPTION_HPP
#define CUTE_BEAST_EXCEPTION_HPP

#include "impl/common.hpp"
#include <boost/beast/http.hpp>
#include <stdexcept>

namespace cute_beast
{

class exception : public std::runtime_error
{
    http::status code_;

public:
    exception(boost::beast::http::status code, std::string body = {})
        : std::runtime_error(body)
        , code_(code)

    {
    }
    http::status status() const
    {
        return code_;
    }
};

} // namespace cute_beast

#endif //CUTE_BEAST_EXCEPTION_HPP
