
#include "../lib/qt/include/server_certificate.hpp"
#include <boost/url/url_view.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_serialize.hpp>
#include <cute_beast/session.hpp>
#include <filesystem>
#include <fmt/format.h>
#include <thread>

namespace
{
using namespace cute_beast;
namespace fs = std::filesystem;

route_ptr make_static_file_handler(fs::path base_path = fs::current_path(), std::string uri_pattern = R"__(^\/(.+)$)__")
{
    return make_route_rule<args::pack<std::string>, http::empty_body>(
        uri_pattern, [=](auto&& session, auto&& request, auto&& path) -> http_generator {
            if (path.find("..") != path.npos) {
                return util::make_bad_request(request, "Illegal request-target");
            }
            http::file_body::value_type file;
            error_code ec;
            auto absolute_path = base_path / path;
            if (fs::is_directory(absolute_path)) {
                //TODO: return files list
                return util::make_bad_request(request, "Illegal request-target");
            }

            file.open(absolute_path.string().c_str(), beast::file_mode::read, ec);
            if (ec) {
                return util::make_not_found(request);
            }
            auto res = util::make_response<http::file_body>(request, http::status::ok, std::move(file));
            res.content_length(fs::file_size(absolute_path));
            return res;
        });
}

} // namespace

template <> struct cute_beast::types::converter<boost::uuids::uuid> {

    converter() = default;
    boost::uuids::uuid convert(std::string&& s, std::error_code& ec)
    {
        try {
            auto result = boost::uuids::string_generator()(s.begin(), s.end());
            ec = {};
            return result;
        } catch (const std::exception& /*e*/) {
            ec = std::make_error_code(std::errc::invalid_argument);
            return {};
        }
    }
};

int main(int argc, char* argv[])
{
    namespace net = cute_beast::net;
    namespace beast = cute_beast::beast;
    using uuid = boost::uuids::uuid;
    std::atomic_uint32_t fail_counter = 0;

    auto ssl_context = std::make_shared<net::ssl::context>(net::ssl::context::tlsv13);
    load_server_certificate(*ssl_context);
    net::io_context ioc;
    cute_beast::error_code ec;
    auto endpoint = net::ip::tcp::endpoint { net::ip::make_address("0.0.0.0"), 4848 };
    auto router = std::make_shared<cute_beast::router>(std::vector<cute_beast::route_ptr> {
        cute_beast::make_route_rule<cute_beast::args::pack<uuid>>(
            R"__(^\/id\/(\{?[0-9a-fA-F]{8}-?[0-9a-fA-F]{4}-?[0-5][0-9a-fA-F]{3}-?[089abAB][0-9a-fA-F]{3}-?[0-9a-fA-F]{12}\}?)$)__",
            [](auto&& session, auto&& request, auto&& id) -> http_generator {
                std::cout << "get uuid: " << id << '\n';
                return util::make_ok(request, std::string("super id: ") + boost::uuids::to_string(id));
            }),
        cute_beast::make_route_rule(R"__(^\/throw_me_plz$)__",
            [&fail_counter](auto&& session, auto&& req) -> http_generator {
                if (fail_counter++ % 2) {
                    throw std::runtime_error("sorry I failed");
                }
                return util::make_ok(req, "I'm okay");
            }),
        make_static_file_handler(),
    });
    auto listener = std::make_shared<cute_beast::listener<decltype(ioc)>>(
        ioc, endpoint, router, cute_beast::session_mode::both_http_https, ssl_context);
    if (!listener->run(ec)) {
        return EXIT_FAILURE;
    };
    std::thread threads[8];
    for (auto& thread : threads) {
        thread = std::thread([&]() { ioc.run(); });
    }
    for (auto& thread : threads) {
        if (thread.joinable()) {
            thread.join();
        }
    }
    return EXIT_SUCCESS;
}
