#include <QCoreApplication>
#include <QDir>
#include <QRegularExpression>
#include <QtBeastServer.h>
#include <QtBeastUtil.hpp>
#include <QtQml/QQmlApplicationEngine>
#include <boost/url/url_view.hpp>
#include <cstdlib>
#include <iostream>
#include <server_certificate.hpp>

namespace beast = boost::beast; // from <boost/beast.hpp>
namespace http = beast::http; // from <boost/beast/http.hpp>
namespace ssl = boost::asio::ssl; // from <boost/asio/ssl.hpp>
namespace asio = boost::asio;
namespace net = boost::asio;

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    QObject guard(&app);
    QQmlApplicationEngine engine("main.qml");
    qDebug() << "engine loaded";
    // The io_context is required for all I/O
    QtBeast::ExecutionContext exec_ctx { qApp };
    QtBeast::Executor executor(&guard, exec_ctx);

    QtBeast::Server server(&app);
    auto ssl_context = std::make_shared<asio::ssl::context>(asio::ssl::context::tlsv13);
    load_server_certificate(*ssl_context);
    server.listen(QHostAddress::Any, 4848, QtBeast::ServerListenMode::both_http_https, ssl_context);
    server.registerStaticDirectoryServer(QDir::currentPath());

    return app.exec();
}
