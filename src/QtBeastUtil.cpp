//
// Created by swex on 1/19/21.
//
#include "QtBeastUtil.hpp"

namespace QtBeast {
namespace {
    ExecutionContext* instance_ = nullptr;
}
EventBase::EventBase()
    : QEvent(generated_type())
{
}

QEvent::Type EventBase::generated_type()
{
    static int event_type = QEvent::registerEventType();
    return static_cast<QEvent::Type>(event_type);
}
ExecutionContext::ExecutionContext(QObject* target)
    : target_(target)
    , filter_()
{
    target_->installEventFilter(&filter_);
    instance_ = this;
}
ExecutionContext::~ExecutionContext()
{
    target_->removeEventFilter(&filter_);
}
ExecutionContext& ExecutionContext::singleton()
{
    assert(instance_);
    return *instance_;
}

Executor::Executor(QObject* contextGuard, ExecutionContext& context) noexcept
    : context_(std::addressof(context))
    , contextGuard_(contextGuard)
{
}

ExecutionContext& Executor::query(boost::asio::execution::context_t) const noexcept
{
    return *context_;
}

bool Executor::operator==(const Executor& other) const noexcept
{
    return context_ == other.context_ && contextGuard_ == other.contextGuard_;
}

bool Executor::operator!=(const Executor& other) const noexcept
{
    return !(*this == other);
}

QtBeast::ExecutionContext::filter::filter()
    : QObject(nullptr)
{
}

bool QtBeast::ExecutionContext::filter::eventFilter(QObject*, QEvent* event)
{
    if (event->type() == EventBase::generated_type()) {
        auto p = static_cast<EventBase*>(event);
        p->accept();
        p->invoke();
        return true;
    } else
        return false;
}

} // namespace QtBeast
