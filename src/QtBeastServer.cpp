#include <QDir>
#include <QFile>
#include <QMimeDatabase>
#include <QMimeType>
#include <QPointer>
#include <QRegularExpression>
#include <QtBeastServer.h>
#include <QtBeastUtil.hpp>
#include <QtCore/QCoreApplication>
#include <QtCore/QEvent>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/execution/allocator.hpp>
#include <boost/asio/execution/context.hpp>
#include <boost/asio/execution/relationship.hpp>
#include <boost/asio/execution_context.hpp>
#include <boost/asio/ip/network_v4.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/noncopyable.hpp>
#include <cute_beast/session.hpp>
#include <filesystem>
#include <string_view>

namespace QtBeast
{

using QFileBody = QIODeviceFileBody<QFile>;

namespace http = boost::beast::http;
namespace beast = boost::beast; // from <boost/beast.hpp>
namespace http = beast::http; // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio; // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp; // from <boost/asio/ip/tcp.hpp>
namespace ssl = boost::asio::ssl; // from <boost/asio/ssl.hpp>
namespace fs = std::filesystem;
using namespace cute_beast;

namespace
{
    // Return a reasonable mime type based on the extension of a file.
    QByteArray mime_type(const QString& path)
    {
        static QMimeDatabase db;
        auto info = db.mimeTypeForFile(path);
        return info.name().toLatin1();
    }

    route_ptr make_static_file_handler(fs::path base_path = fs::current_path(), std::string uri_pattern = R"__(^\/(.+)$)__")
    {
        return make_route_rule<args::pack<std::string>, http::empty_body>(uri_pattern, [=](auto&& session, auto&& request, auto&& path) {
            using util::make_gen;
            if (path.find("..") != path.npos) {
                return util::make_gen(util::make_bad_request(request, "Illegal request-target"));
            }
            QFileBody::value_type file;
            beast::error_code ec;
            auto absolute_path = base_path / path;
            if (fs::is_directory(absolute_path)) {
                //TODO: return files list
                return util::make_gen(util::make_bad_request(request, "Illegal request-target"));
            }

            file.open(QString::fromStdString(absolute_path.string()), QIODevice::ReadOnly, ec);
            if (ec) {
                return util::make_gen(util::make_not_found(request));
            }
            http::response<QIODeviceFileBody<QFile>> res;
            res.body() = std::move(file);
            auto mime = mime_type(QString::fromStdString(path));
            res.set(http::field::content_type, cute_beast::string_view(mime.data(), mime.size()));
            res.content_length(fs::file_size(absolute_path));
            //            beast::http::async_write({}, res, [](auto e, auto b) {});
            return util::make_gen(std::move(res));
        });
    }
} // namespace
struct ServerPrivate {
    Server* q_ptr;
    Q_DECLARE_PUBLIC(Server)
    std::shared_ptr<ExecutionContext> exec_ctx_;
    std::shared_ptr<Executor> io_executor_;
    net::any_io_executor type_erased_executor_;
    std::shared_ptr<router> router_;

public:
    explicit ServerPrivate(Server* thiz)
        : q_ptr(thiz)
        , exec_ctx_(std::make_shared<ExecutionContext>(thiz))
        , io_executor_(std::make_shared<Executor>(thiz, *exec_ctx_))

    {
        type_erased_executor_ = *io_executor_;
    }
    bool listen(const QHostAddress& address, quint16 port, ServerListenMode mode, std::shared_ptr<boost::asio::ssl::context> ssl_ctx)
    {

        router_ = std::make_shared<router>(std::vector<route_ptr> {});
        router_->add_rule<args::pack<std::string>>(R"__(^\/user\/([A-Za-z0-9]+)$)__", [](auto&& session, auto&& req, auto&& user_id) {
            auto responese = util::make_response<http::string_body>(req, http::status::ok, std::string("user: ") + user_id + " returned");
            return responese;
        });
        router_->add_websocket_rule(R"__(/websocket/example)__", [](std::shared_ptr<session::websocket> websocket) {
            std::cerr << "add_websocket_rule \n";

            struct ws_echo_worker : public std::enable_shared_from_this<ws_echo_worker> {
                std::shared_ptr<session::websocket> websocket_;
                std::string string_;
                decltype(net::dynamic_buffer(string_)) buf_;
                explicit ws_echo_worker(std::shared_ptr<session::websocket> ws)
                    : websocket_(std::move(ws))
                    , buf_(string_)
                {
                }
                void on_read(error_code ec, size_t br, bool is_text)
                {
                    if (ec) {
                        //                        std::cerr << "async_read failed: " << ec.message() << '\n';
                        return;
                    }
                    //                    if (br) {
                    //                        if (is_text) {
                    //                            std::cerr << "got text msg of size: " << br << " : " << std::string_view(string_.data(), br) << '\n';
                    //                        } else {
                    //                            std::cerr << "got bin msg of size: " << br << '\n';
                    //                        }
                    //                    }
                    websocket_->async_write(
                        buf_.data(), is_text, [self = shared_from_this()](auto&& ec, auto&& bw) { self->on_write(ec, bw); });
                };
                void on_write(error_code ec, size_t bw)
                {
                    if (ec) {
                        return;
                    }
                    buf_.consume(bw);
                    run();
                }
                void run()
                {
                    websocket_->async_read(
                        buf_, [self = shared_from_this()](auto&& ec, auto&& br, auto&& is_text) { self->on_read(ec, br, is_text); });
                }
            };
            std::make_shared<ws_echo_worker>(std::move(websocket))->run();
        });

        constexpr auto convertMode = [](ServerListenMode mode) {
            if (mode == ServerListenMode::http)
                return session_mode::http;
            if (mode == ServerListenMode::https)
                return session_mode::https;
            return session_mode::both_http_https;
        };
        auto endpoint = tcp::endpoint { net::ip::make_address(address.toString().toStdString()), port };
        auto listenerz = std::make_shared<listener<decltype(type_erased_executor_)>>(
            type_erased_executor_, endpoint, router_, convertMode(mode), std::move(ssl_ctx));
        beast::error_code ec;
        auto ret = listenerz->run(ec);
        return ret;
    }
};

Server::Server(QObject* parent)
    : QObject(parent)
    , q_ptr(std::make_unique<ServerPrivate>(this))
{
}

bool Server::listen(const QHostAddress& address, quint16 port, ServerListenMode mode, std::shared_ptr<boost::asio::ssl::context> ssl_ctx)
{
    return q_ptr->listen(address, port, mode, std::move(ssl_ctx));
}

bool Server::isListening() const
{
    return false;
}

void Server::registerStaticDirectoryServer(QString directoryPath, QString target_regex)
{
    q_ptr->router_->add_rule(make_static_file_handler(fs::path(directoryPath.toStdString()), target_regex.toStdString()));
}
size_t Server::deregisterPath(QString target_regex)
{
    return q_ptr->router_->remove_matched(target_regex.toStdString());
}
Server::~Server()
{
}

} // namespace QtBeast
